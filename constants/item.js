import React, { useState } from 'react';
import { StyleSheet, Text, View, FlatList, Image, TouchableOpacity, Dimensions } from 'react-native';
import { colors } from 'react-native-elements';
import { color } from 'react-native-elements/dist/helpers';
import LinearGradient from 'react-native-linear-gradient';
import { IconButton } from 'react-native-paper';
import { backgroundColor } from 'styled-system';

const Item = ({ itemData, onPress, navigation }) => {
  return (
    <LinearGradient colors={['#F9FBFF', '#BED3FF']} style={styles.listItem}>

      <TouchableOpacity onPress={onPress} style={styles.listItem}>
        <TouchableOpacity style={{
          height: 20, width: 50, justifyContent: "space-evenly",
          alignItems: "center",
          flexDirection: 'row-reverse'
        }}
          onPress={onPress}
        >
          <Text style={{ color: "blue" }}>Bizerte</Text>
          <IconButton
            icon="heart"
            color='#C0C0C0'
            size={25}
            onPress={() => console.log('Pressed')}
          />
        </TouchableOpacity>
        <View style={{
          alignItems: "flex-end", flex: 1, padding: 2, marginRight: 12,
          justifyContent: "space-between",
        }}>
          <TouchableOpacity style={{
            height: 20, width: 50, justifyContent: "flex-start", alignItems: "center",
            flexDirection: 'row-reverse',
          }}

          >
            <Text style={{ color: "blue" }}>Bizerte</Text>
            <IconButton
              icon="heart"
              color='#C0C0C0'
              size={25}
              onPress={() => console.log('Pressed')}
            />
          </TouchableOpacity>
          <Text style={{ fontWeight: "bold" }}>{itemData.name}</Text>
          <Text>{itemData.position}</Text>
        </View>

        <Image source={{ uri: itemData.photo }} style={{ width: 90, height: 80, borderRadius: 30, backgroundColor: 'transparent' }} />

      </TouchableOpacity>
    </LinearGradient>

  );
}
export default Item
const styles = StyleSheet.create({

  listItem: {
    margin: 10,
    padding: 10,
    height: 100,
    minWidth: Dimensions.get("screen").width,
    backgroundColor: 'transparent',
    width: "80%",
    flex: 1,
    alignSelf: "center",
    flexDirection: "row",
    borderRadius: 5
  }
});