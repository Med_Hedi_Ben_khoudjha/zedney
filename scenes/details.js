import React from "react";

import { useState } from "react"
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import LinearGradient from 'react-native-linear-gradient';
import { IconButton } from "react-native-paper";

const Detail = ({ navigation, route }) => {
    const [data, setData] = useState();
    const { itemData } = route.params;


    console.log(JSON.stringify(itemData.name))

    const ra = JSON.stringify(itemData.position);


    return (
        <LinearGradient colors={['#F9FBFF', '#BED3FF']} style={styles.listItem}>

            <TouchableOpacity onPress={() => console.log("zedney")}
                style={styles.listItem}>
                <TouchableOpacity style={{
                    height: 20, width: 50, justifyContent: "space-evenly",
                    alignItems: "center",
                    flexDirection: 'row-reverse'
                }}
                >
                    <Text style={{ color: "blue" }}>Bizerte</Text>
                    <IconButton
                        icon="heart"
                        color='#C0C0C0'
                        size={25}
                        onPress={() => console.log('Pressed')}
                    />
                </TouchableOpacity>
                <View style={{
                    alignItems: "flex-end", flex: 1, padding: 2, marginRight: 12,
                    justifyContent: "space-between",
                }}>
                    <TouchableOpacity style={{
                        height: 20, width: 50, justifyContent: "flex-start", alignItems: "center",
                        flexDirection: 'row-reverse',
                    }}

                    >
                        <Text style={{ color: "blue" }}>Bizerte</Text>
                        <IconButton
                            icon="heart"
                            color='#C0C0C0'
                            size={25}
                            onPress={() => console.log('Pressed')}
                        />
                    </TouchableOpacity>
                    <Text style={{ fontWeight: "bold" }}>{itemData.name}</Text>
                    <Text>{itemData.position}</Text>
                </View>

                <Image source={{ uri: itemData.photo }} style={{ width: 90, height: 80, borderRadius: 30, backgroundColor: 'transparent' }} />

            </TouchableOpacity>
        </LinearGradient>

    );

}
export default Detail

const styles = StyleSheet.create({

    listItem: {
        marginRight: 30,
        padding: 20,
        height: 100,
        minWidth: Dimensions.get("screen").width,
        backgroundColor: 'transparent',
        width: "100%",
        flex: 1,
        alignSelf: "auto",
        flexDirection: "row",
        borderRadius: 5
    }
});