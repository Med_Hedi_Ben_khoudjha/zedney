import React from "react";

import { Container, } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { IconButton, Colors, Card } from 'react-native-paper';
import { FlatList, ScrollView, StatusBar, StyleSheet, Text, TextInput, TouchableOpacity, View, } from "react-native";
import Item from '../constants/item'
import { Input } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { backgroundColor } from "styled-system";
const Home = ({ navigation }) => {
    const [blue, setBlue] = React.useState(false);

    const [appart, setAppart] = React.useState([
        {
            "name": "Miyah Myles",
            "email": "miyah.myles@gmail.com",
            "position": "Data Entry Clerk",
            "photo": "https:\/\/images.unsplash.com\/photo-1494790108377-be9c29b29330?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&s=707b9c33066bf8808c934c8ab394dff6"
        },
        {
            "name": "June Cha",
            "email": "june.cha@gmail.com",
            "position": "Sales Manager",
            "photo": "https:\/\/randomuser.me\/api\/portraits\/women\/44.jpg"
        },
        {
            "name": "Iida Niskanen",
            "email": "iida.niskanen@gmail.com",
            "position": "Sales Manager",
            "photo": "https:\/\/randomuser.me\/api\/portraits\/women\/68.jpg"
        },
        {
            "name": "Renee Sims",
            "email": "renee.sims@gmail.com",
            "position": "Medical Assistant",
            "photo": "https:\/\/randomuser.me\/api\/portraits\/women\/65.jpg"
        },
        {
            "name": "Jonathan Nu\u00f1ez",
            "email": "jonathan.nu\u00f1ez@gmail.com",
            "position": "Clerical",
            "photo": "https:\/\/randomuser.me\/api\/portraits\/men\/43.jpg"
        },
        {
            "name": "Sasha Ho",
            "email": "sasha.ho@gmail.com",
            "position": "Administrative Assistant",
            "photo": "https:\/\/images.pexels.com\/photos\/415829\/pexels-photo-415829.jpeg?h=350&auto=compress&cs=tinysrgb"
        },
        {
            "name": "Abdullah Hadley",
            "email": "abdullah.hadley@gmail.com",
            "position": "Marketing",
            "photo": "https:\/\/images.unsplash.com\/photo-1507003211169-0a1dd7228f2d?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&s=a72ca28288878f8404a795f39642a46f"
        },
        {
            "name": "Thomas Stock",
            "email": "thomas.stock@gmail.com",
            "position": "Product Designer",
            "photo": "https:\/\/tinyfac.es\/data\/avatars\/B0298C36-9751-48EF-BE15-80FB9CD11143-500w.jpeg"
        },
        {
            "name": "Veeti Seppanen",
            "email": "veeti.seppanen@gmail.com",
            "position": "Product Designer",
            "photo": "https:\/\/randomuser.me\/api\/portraits\/men\/97.jpg"
        },
        {
            "name": "Bonnie Riley",
            "email": "bonnie.riley@gmail.com",
            "position": "Marketing",
            "photo": "https:\/\/randomuser.me\/api\/portraits\/women\/26.jpg"
        }
    ]);
    const [data, setData] = React.useState([
        { value: "Bizerte", key: 1 },
        { value: "Tunis", key: 2 },
        { value: "Beja", key: 3 },
        { value: "Nabel", key: 4 },
        { value: "Sfax", key: 5 },

    ]);
    const renderItem = ({ item, index }) => {

        return (
            <Item
                itemData={item}
                key={item.email}
                onPress={() => navigation.navigate('Detail', {
                    itemData: item,
                    otherParam: 'anything you want here',
                })}
            />
        );
    };


    return (

        <Container style={{ backgroundColor: '#fff' }}>
            <StatusBar hidden={true} />

            <View style={{ marginTop: 60, flexDirection: "row", justifyContent: 'space-between', padding: 1 }}>
                <IconButton
                    icon="format-list-bulleted"
                    color='#BED3FF'
                    size={35}
                    onPress={() => console.log('Pressed')}
                />
                <View style={styles.MainContainer}>

                    <Input
                        //  accessibilityLabel='med'

                        underlineColorAndroid='transparent'
                        placeholder='INPUT WITH CUSTOM ICON'
                        containerStyle={{
                            borderWidth: 1,
                            borderRadius: 12,
                            height: 50,
                            borderColor: '#C0C0C0'
                        }}
                        inputStyle={{}}
                        rightIcon={
                            <Icon
                                name='search'
                                size={24}
                                color='#C0C0C0'
                            />
                        }
                    />



                </View>




            </View>
            <View>
                <View

                    style={{ marginTop: 10 }}
                >
                    <FlatList
                        data={data}
                        horizontal
                        contentContainerStyle={{ backgroundColor: 'rgba(255, 255, 255, 0.0)' }}
                        showsHorizontalScrollIndicator={false}
                        renderItem={({ item }) =>
                            <TouchableOpacity

                                key={item.key}
                                style={{
                                    ...styles.item,

                                }}
                            >
                                <Text style={{
                                    fontWeight: "bold",
                                    fontSize: 15,
                                    textAlign: 'center',
                                    padding: 10,

                                }} >{item.value}</Text>
                            </TouchableOpacity>}
                        keyExtractor={item => item.key}
                    />


                </View>
                <View style={styles.container}>
                    <FlatList
                        // style={{ flex: 1 }}
                        data={appart}
                        style={{ backgroundColor: '#fff' }}
                        renderItem={(item) => renderItem(item)}
                        keyExtractor={item => item.email}
                    />
                </View>

            </View>
        </Container>

    );
}
export default Home


const styles = StyleSheet.create({

    MainContainer: {
        // Setting up View inside content in Vertically center.
        justifyContent: 'flex-start',
        flex: 1,
        margin: 10
    }, linearGradient: {
        flex: 1,

    },
    container: {
        // flex: 1,
        backgroundColor: '#F7F7F7',
        marginTop: 20
    }, item: {
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: .3,
        shadowRadius: 20,
        alignItems: "center",
        flexDirection: "row",
        backgroundColor: "#fff",
        marginHorizontal: 15, borderWidth: 1,
        borderRadius: 100 / 2,
        height: 60,
        width: 95,
        paddingVertical: 5,
        paddingHorizontal: 15
    }

});