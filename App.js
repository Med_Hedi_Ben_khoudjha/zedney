import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import Nav from "./routes/nav";
import SplashScreen from 'react-native-splash-screen';


const { View, Text } = require("react-native")

const App = () => {
  React.useEffect(() => {
    SplashScreen.hide();
    //
    //  setCount(CartL.length)
  }, []);

  return (

    <NavigationContainer>
      <Nav></Nav>
    </NavigationContainer>

  )
}
export default App;