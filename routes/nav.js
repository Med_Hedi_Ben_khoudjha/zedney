import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Home from '../scenes/Home';
import Detail from "../scenes/details";

import { createStackNavigator } from '@react-navigation/stack';




function Post() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Post!</Text>
            <Image
                source={require('../assets/plus.png')}
                resizeMode="contain" style={{
                    width: 50,
                    height: 50,
                    //tintColor: '#fff'
                }} />
        </View>
    );
}

function SettingsScreen() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Settings!</Text>
        </View>
    );
}

const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();


const CustomTabBarButton = ({ children, onPress }) =>
(
    <TouchableOpacity
        onPress={onPress}
        style={{
            top: -30,
            justifyContent: 'center',
            alignItems: 'center',
            ...styles.shadow
        }}>
        <View
            style={{
                width: 70,
                height: 70,
                borderRadius: 35,
                backgroundColor: '#FFFF'
            }}>
            {children}
        </View>
    </TouchableOpacity>
);


const Nav = () => {
    return (
        <Tab.Navigator initialRouteName="Home"

            screenOptions={{
                tabBarActiveTintColor: '#C0C0C0',
                tabBarActiveTintColor: '#BED3FF',

                tabBarShowLabel: false,
                tabBarStyle: {

                    position: 'absolute',
                    backgroundColor: '#FFFFFF',
                    borderRadius: 5,
                    height: 60,
                    elevation: 0,
                    ...styles.shadow
                }
            }}>
            <Tab.Screen
                name="HomeScreen"

                component={HomeStackScreen} options={{
                    headerShown: false,
                    tabBarIcon: ({ color, size }) => (
                        <Icon name="person-outline" color={color} size={size} />
                    ),
                }} />
            <Tab.Screen name="Settings" component={SettingsScreen} options={{
                // tabBarLabel: 'Home',
                tabBarIcon: ({ color, size }) => (
                    <Icon name="chat" color={color} size={size} />
                ),
            }}
            /><Tab.Screen name="Post" component={Post}
                options={{
                    // tabBarLabel: 'Home',
                    tabBarIcon: ({ focused }) => (

                        <Image
                            source={require('../assets/plus.png')}
                            resizeMode="contain" style={{
                                width: 68,
                                height: 68,
                                // tintColor: '#000'
                            }} />

                    )
                    , tabBarButton: (props) =>
                    (<CustomTabBarButton{...props} />
                    )

                }}
            /><Tab.Screen name="Profil" component={SettingsScreen} options={{
                // tabBarLabel: 'Home',
                tabBarIcon: ({ color, size }) => (
                    <Icon name="volume-up" color={color} size={size} />
                ),
            }}
            /><Tab.Screen name="M" component={SettingsScreen} options={{
                // tabBarLabel: 'Home',
                tabBarIcon: ({ color, size }) => (
                    <Icon name="home" color={color} size={size} />
                ),
            }}
            />
        </Tab.Navigator>
    );
}

const HomeStackScreen = ({ navigation }) => (
    <HomeStack.Navigator
        initialRouteName="Home"


    >
        <HomeStack.Screen name="Home" component={Home} options={{
            tabBarLabel: 'Home',

            headerTransparent: true,
            headerTitle: '',
            unmountOnBlur: true,
            headerLeft: () => (
                <View style={{
                    // Try setting `flexDirection` to `"row"`.
                    flexDirection: "row", justifyContent: 'space-between', padding: 10
                }}>
                    <Icon name="settings" color='#C0C0C0' size={30} />
                    <Icon name="notifications" color='#C0C0C0' size={30} style={{ marginLeft: 10 }} />
                </View>


            ),
            headerRight: () => (
                <View style={{
                    // Try setting `flexDirection` to `"row"`.
                    flexDirection: "row", justifyContent: 'space-between', padding: 10
                }}>
                    <Image
                        source={require('../assets/logo.jpg')}
                        resizeMode="contain" style={{
                            width: 80,
                            height: 80,
                            //tintColor: '#fff'
                        }} /></View>


            ),


        }} />

        <HomeStack.Screen
            name="Detail"
            component={Detail}
            options={({ route }) => ({
                //title: route.params.title,
                headerShown: true,
                title: ''
            })}

        />

    </HomeStack.Navigator>
);


export default Nav
const styles = StyleSheet.create({
    shadow: {
        shadowColor: '#7F5DF0',

        shadowOffset: {
            width: 0,
            height: 10
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.5,
        elevation: 5
    }


});
